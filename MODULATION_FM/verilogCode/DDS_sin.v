module DDS_sin
(
	input clk_in_100m,
	input rst_n,
	input [31:0] fre_word_in,

	output [13:0] dacdata
);

/* wire define */
wire [11:0] rom_addr;

/* reg define */
reg [31:0] addr;

always @(posedge clk_in_100m or negedge rst_n)
begin
	if(rst_n == 0)
	begin
		addr <= 32'b0;
	end
	else
		addr <= addr + fre_word_in;
end

assign rom_addr[11:0] = addr[31:20];

ROM_sin	ROM_sin_inst (
	.address ( rom_addr ),
	.clock ( clk_in_100m ),
	.q ( dacdata )
	);
	
endmodule
