module MODULATION_ask
(
	input clk_in_50m,
	input rst_n_in,
	input [7:0] sig_key,
	input en_sigkey_read,
	
	output reg [13:0] ASK2_out,
	output clk_out_100m	
);

/* parameter define */
parameter carrier_freword = 32'd4294967;//100KHz
parameter A = 14'd8191;

/* wire define */
wire [13:0] carrier_100k;
wire sig_keycon;
wire clk_100m;
wire pll_locked;
wire rst_n;
wire [13:0] ASK2_temp;

/* reg define */


assign rst_n = pll_locked & rst_n_in;
assign clk_out_100m = clk_100m;

mult	mult_inst (
	.dataa ( carrier_100k ),
	.datab ( sig_keycon ),
	.result ( ASK2_temp )
	);


always @(posedge carrier_100k or negedge rst_n)
begin
	if(rst_n == 0)
		ASK2_out <= A;
	else
		if(ASK2_temp == 0)
			ASK2_out <= ASK2_temp + A;
		else
			ASK2_out <= ASK2_temp;
end

PLL	PLL_inst (
	.inclk0 ( clk_in_50m ),
	.c0 ( clk_100m ),
	.locked ( pll_locked )
	);
	
DDS_sin carrier(
	.clk_in_100m(clk_100m),
	.rst_n(rst_n),
	.fre_word_in(carrier_freword),
	.dacdata(carrier_100k)
);

ParallelToSerial P2S_inst(
	.clk_in_50m(clk_in_50m),
	.para_in(sig_key),//[7:0]
	.en(en_sigkey_read),
	.rst_n(rst_n),
	.ser_out(sig_keycon)
);
endmodule
