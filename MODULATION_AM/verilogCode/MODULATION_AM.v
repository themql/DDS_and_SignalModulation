module MODULATION_AM
(
	input clk_in_50m,
	input rst_n_in,
	input [3:0] depth_in,
	input [31:0] carrier_freword_in,

	output [13:0] modulation_AM_result,
	output clk_out_100m
);

/* parameter defne */
parameter MODULATION_FREWORD = 32'd42950;//1kHz

/* wire define */
wire clk_100m;
wire pll_locked;
wire rst_n;
wire [15:0] sig_14;
wire [7:0] sig_7;
wire [7:0] signal_modulator;
wire [7:0] signal_carrier;
wire [15:0] modulation_AM_temp;

/* reg define */
reg [7:0] A = 7'd63;
reg [7:0] depth_con;
reg [7:0] modulation_withDC;

assign rst_n = pll_locked & rst_n_in;
assign clk_out_100m = clk_100m;

always @(posedge clk_100m)
begin
	case (depth_in)
		1 : depth_con <= 7'd6;
		2 : depth_con <= 7'd13;
		3 : depth_con <= 7'd19;
		4 : depth_con <= 7'd26;
		5 : depth_con <= 7'd32;
		6 : depth_con <= 7'd38;
		7 : depth_con <= 7'd45;
		8 : depth_con <= 7'd51;
		9 : depth_con <= 7'd58;
		10: depth_con <= 7'd64;
	endcase
end

/* ma*cos(w0t) */
mult	mult_inst_1 (
	.dataa ( signal_modulator ),
	.datab ( depth_con ),
	.result ( sig_14 )
	);

assign sig_7 = sig_14 >> 6;

/* A+ma*cos(w0t) */
always @(posedge clk_100m)
begin
	modulation_withDC <= sig_7 + A;
end

/* （A+ma*cos(w0t)）*cos（wct） */
mult	mult_inst_2 (
	.dataa ( modulation_withDC ),
	.datab ( signal_carrier ),
	.result ( modulation_AM_temp )
	);

assign modulation_AM_result = modulation_AM_temp[13:0] + 14'd8191;
	
PLL	PLL_inst (
	.inclk0 ( clk_in_50m ),
	.c0 ( clk_100m ),
	.locked ( pll_locked )
	);

DDS_sin DDS_sin_modulator (
	.clk_in_100m(clk_100m),
	.rst_n(rst_n),
	.fre_word_in(MODULATION_FREWORD),
	.dacdata(signal_modulator)
);

DDS_sin DDS_sin_carrier (
	.clk_in_100m(clk_100m),
	.rst_n(rst_n),
	.fre_word_in(carrier_freword_in),
	.dacdata(signal_carrier)
);

endmodule
