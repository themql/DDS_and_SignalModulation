module AM_top
(
	input clk_in_50m,
   input rst_n_in,
	input key_1_in,
	input key_2_in,
	input key_3_in,

   output [13:0] dacdata_out,
	output clk_out_100m,
);

/* parameter define */


/* wire define */
wire [3:0] depth_data;
wire [31:0] fre_word_data;

/* reg define */


KEY_scan KEY_scan_inst(
    .clk_in_50m(clk_in_50m),
    .rst_n(rst_n_in),
	.key_1(key_1_in),
	.key_2(key_2_in),
	.key_3(key_3_in),
	.key_4(),
	.depth_out(depth_data),//output [3:0]
	.fre_word_out(fre_word_data)//output [31:0]
);

MODULATION_AM MODULATION_AM_inst(
   .clk_in_50m(clk_in_50m),
   .rst_n_in(rst_n_in),
   .depth_in(depth_data),//input [3:0]
   .carrier_freword_in(fre_word_data),//input [31:0]
   .modulation_AM_result(dacdata_out),//output [13:0]
	.clk_out_100m(clk_out_100m)
);

endmodule
