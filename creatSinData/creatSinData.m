clc;

depth = 4096;
width = 14;
offset = 0;

x = linspace(0, 2*pi, depth);
y = sin(x) + offset;
yData = ceil(y*(pow2(width-1)-1));

fid = fopen('sin_width14_offset0.mif', 'wt');
%head
fprintf(fid, 'DEPTH = %d;\n', depth);
fprintf(fid, 'WIDTH = %d;\n', width);
fprintf(fid, 'ADDRESS_RADIX = DEC;\n');
fprintf(fid, 'DATA_RADIX = DEC;\n');
fprintf(fid, 'CONTENT\n');
fprintf(fid, 'BEGIN\n');
%add : data
for i = 1: 1: depth
    fprintf(fid, '%d : %d;\n', i-1, yData(i));
end
fprintf(fid, 'END\n');
fclose(fid);

plot(x, yData);