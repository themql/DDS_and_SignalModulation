# DDS与信号调制

### 文件说明

* creatSinData

	生成rom初始化用的mif文件。

* DDS

	频率可调（1KHz~10MHz）的正弦信号发生器（外接DAC）

* MODULATION_AM

	1KHz正弦调制信号，载波频率1MHz~10MHz（步进量1MHz），调制度10%~100%（步进量10%）

* MODULATION_ASK2

	100KHz载波，10Kbps码率，二进制基带序列信号由8个引脚并转串产生。
	存在的问题：载波的频率有抖动，无法与二进制信号精准吻合。

* MODULATION_FM

	1KHz正弦调制信号，载波频率100KHz~10MHz，10KHz最大偏频

* MODULATION_PSK2

	同ASK2，也存在相同问题。

* STM32Control

	用于产生8位二进制信号，与并转串读取信号。

### 参考资料

##### 题目
https://wenku.baidu.com/view/b51e3a25ccbff121dd3683bf.html

##### DDS
https://blog.csdn.net/qq_41754003/article/details/107444161
https://blog.csdn.net/MDYFPGA/article/details/104754953

##### 按键 参考
https://wenku.baidu.com/view/ae7b64c74a7302768f993952.html

##### AM 参考
https://blog.csdn.net/huangshangcheng/article/details/80628867

##### ASK 参考
https://blog.csdn.net/FPGADesigner/article/details/80710344

##### PSK参考
https://wenku.baidu.com/view/2d84662cf11dc281e53a580216fc700abb68528b.html

##### FM参考
https://blog.csdn.net/hooknet/article/details/81278232