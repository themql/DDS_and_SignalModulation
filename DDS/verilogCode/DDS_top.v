module DDS_top
(
	input clk_in_50m,
	input rst_n_in,
	input key_1_in,
	input key_2_in,
	input key_3_in,
	
	output clk_out_100m,
	output [13:0] dacdata
);

/* wire define */
wire clk_100m;
wire [31:0] fre_word;
wire pll_locked;
wire rst_n;

/* reg define */


assign clk_out_100m = clk_100m;
assign rst_n = rst_n_in & pll_locked;

PLL	PLL_inst (
	.inclk0 ( clk_in_50m ),
	.c0 ( clk_100m ),
	.locked ( pll_locked )
	);

KEY_scan KEY_scan_inst(
	.clk_in_50m(clk_in_50m),
	.rst_n(rst_n),
	.key_1(key_1_in),
	.key_2(key_2_in),
	.key_3(key_3_in),
	.key_4(),
	.fre_word_out(fre_word)
	);

DDS_sin DDS_sin_inst(
	.clk_in_100m(clk_100m),
	.rst_n(rst_n),
	.fre_word_in(fre_word),
	.dacdata(dacdata)
	);
endmodule
