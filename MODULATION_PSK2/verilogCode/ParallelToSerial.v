module ParallelToSerial
(
    input clk_in_50m, 
    input [7:0] para_in,
    input en,
    input rst_n,

    output reg ser_out
);

/* parameter define */

/* wire define */

/* reg define */
reg [6:0] tmp;
reg [11:0] cnt;
reg clk_10k;

always @(posedge clk_in_50m or negedge rst_n)
begin
    if(rst_n == 0)
    begin
        cnt <= 0;
        clk_10k <= 0;
    end
    else if(cnt == 12'b100111000011)//2499  div5000
    begin
        cnt <= 0;
        clk_10k <= ~clk_10k;
    end
    else
        cnt <= cnt +12'b1;
end

always @(posedge clk_10k or negedge rst_n)
begin
    if(rst_n == 0)
        {ser_out, tmp} <= 8'b0;
    else
        if(en)
            {ser_out, tmp} <= para_in;
        else
            {ser_out, tmp} <= {tmp, ser_out};
end

endmodule
