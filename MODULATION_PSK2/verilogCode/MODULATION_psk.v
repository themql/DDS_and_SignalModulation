module MODULATION_psk
(
	input clk_in_50m,
	input rst_n_in,
	input [7:0] sig_key,
	input en_sigkey_read,
	
	output [13:0] PSK2_out,
	output clk_out_100m
);

/* parameter define */
parameter carrier_freword = 32'd4294980;//100KHz

/* wire define */
wire [13:0] carrier_100k;
//wire [13:0] carrier_100k_reverse;
wire sig_keycon;
wire clk_100m;
wire pll_locked;
wire rst_n;
wire [15:0] mult_result;

/* reg define */
reg [1:0] sig_keycon_signed;

assign rst_n = pll_locked & rst_n_in;
assign clk_out_100m = clk_100m;
//assign carrier_100k_reverse = ~carrier_100k;
assign PSK2_out = mult_result[13:0];

always @(posedge carrier_100k or negedge rst_n)
begin
	if(rst_n == 0)
		sig_keycon_signed <= 2'b00;
	else
		if(sig_keycon == 0)
			sig_keycon_signed <= 2'b11;
		else
			sig_keycon_signed <= 2'b01;
end

/*
MUX21	MUX21_inst (
	.data0x ( carrier_100k ),
	.data1x ( carrier_100k_reverse ),
	.sel ( sig_keycon ),
	.result ( PSK2_out )
	);
*/

MULT	MULT_inst (
	.dataa ( carrier_100k ),
	.datab ( sig_keycon_signed ),
	.result ( mult_result )
	);
	
PLL	PLL_inst (
	.inclk0 ( clk_in_50m ),
	.c0 ( clk_100m ),
	.locked ( pll_locked )
	);

DDS_sin carrier(
	.clk_in_100m(clk_100m),
	.rst_n(rst_n),
	.fre_word_in(carrier_freword),
	.dacdata(carrier_100k)
);

ParallelToSerial P2S_inst(
	.clk_in_50m(clk_in_50m),
	.para_in(sig_key),//[7:0]
	.en(en_sigkey_read),
	.rst_n(rst_n),
	.ser_out(sig_keycon)
);

endmodule
